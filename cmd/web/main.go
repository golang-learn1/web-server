package main

import (
	"flag"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/golang-learn1/web-server/pkg/models/mysql"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

type neuteredFileSystem struct {
	fs http.FileSystem
}

func (nfs neuteredFileSystem) Open(path string) (http.File, error) {
	f, err := nfs.fs.Open(path)
	if err != nil {
		return nil, err
	}

	s, err := f.Stat()
	if s.IsDir() {
		index := filepath.Join(path, "index.html")
		if _, err := nfs.fs.Open(index); err != nil {
			if closeErr := f.Close(); closeErr != nil {
				return nil, closeErr
			}
			return nil, err
		}
	}
	return f, nil
}

func main() {
	app := &application{}
	app.infoLog = log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	app.errorLog = log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)
	addr := flag.String("addr", ":4000", "The port our server is listening to.")
	dsn := flag.String("dsn", "web:pass@/snippetbox?parseTime=true", "Название mysql источника данных")


	srv := &http.Server{
		Addr:     *addr,
		ErrorLog: app.errorLog,
		Handler:  app.routes(),
	}
	flag.Parse()

	db,err := openDb(*dsn)
	defer db.Close()
	if err != nil {
		app.errorLog.Fatal(err)
	}
	app.snippets = &mysql.SnippetModel{DB: db}

	app.infoLog.Printf("Запуск сервера на %s", srv.Addr)
	err = srv.ListenAndServe()
	app.errorLog.Fatal(err)
}

