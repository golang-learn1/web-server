package main

import (
	"gitlab.com/golang-learn1/web-server/pkg/models/mysql"
	"log"
)

type application struct {
	errorLog *log.Logger
	infoLog *log.Logger
	snippets *mysql.SnippetModel
}