package main

import "net/http"

func (app *application) routes() *http.ServeMux{
	fileServer := http.FileServer(neuteredFileSystem{http.Dir("./ui/static")})
	mux := http.NewServeMux()

	mux.HandleFunc("/", app.home)
	mux.HandleFunc("/snippet", app.showSnippet)
	mux.HandleFunc("/snippet/create", app.createSnippet)
	mux.HandleFunc("/downloadhandler", downloadHandler)
	mux.Handle("/static", http.NotFoundHandler())
	mux.Handle("/static/", http.StripPrefix("/static", fileServer))
	return mux
}